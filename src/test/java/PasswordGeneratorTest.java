import com.progressoft.passwordGenerator.PasswordGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

public class PasswordGeneratorTest {

    @Test
    public void givenPasswordGenerator_whenGenerate_thenValidateResult() {
        PasswordGenerator passwordGenerator = new PasswordGenerator();
        String string = passwordGenerator.generatePassword();
        char[] password = string.toCharArray();

        Assertions.assertEquals(8, password.length, "password length not as expected");

        char key;
        int numbersCount = 0;
        int symbolsCount = 0;
        int lettersCount = 0;
        Set<Character> allowedSymbols = new HashSet<>();
        allowedSymbols.add('_');
        allowedSymbols.add('$');
        allowedSymbols.add('#');
        allowedSymbols.add('%');

        for (int i = 0; i < password.length; i++) {
            key = password[i];
            if (inInvalidCharacter(key))
                Assertions.fail("unexpected character");

            if (isLetter(key))
                lettersCount++;
            else if (isNumber(key))
                numbersCount++;
            else
                symbolsCount++;

            for (int j = i + 1; j < password.length; j++)
                if (key == password[j])
                    Assertions.fail("duplications are not allowed");

            //Assert shuffling
            if (isNumberDigit(i))
                Assertions.assertTrue(isNumber(password[i]));
            if (isSymbolDigit(i))
                Assertions.assertTrue(allowedSymbols.contains(password[i]));
            if (isLetterDigit(i))
                Assertions.assertTrue(isLetter(password[i]));

        }

        Assertions.assertEquals(4, numbersCount, "there should be 4 numbers");
        Assertions.assertEquals(2, symbolsCount, "there should be 2 symbols");
        Assertions.assertEquals(2, lettersCount, "there should be 2 letters");

    }

    private static boolean isNumber(char key) {
        return key >= '0' && key <= '9';
    }

    private static boolean isLetter(char key) {
        return key >= 'A' && key <= 'Z';
    }

    private static boolean inInvalidCharacter(char key) {
        return !(isLetter(key) || (key >= '0' && key <= '9') || (key == '%' || key == '$' || key == '_' || key == '#'));
    }

    private static boolean isLetterDigit(int i) {
        return (i % 2) != 0 && i > 3;
    }

    private static boolean isSymbolDigit(int i) {
        return (i % 2) != 0 && i <= 3;
    }

    private static boolean isNumberDigit(int i) {
        return i % 2 == 0;
    }
}
