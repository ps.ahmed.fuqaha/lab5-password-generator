package com.progressoft.passwordGenerator;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> passwords = new HashSet<>();
        String password;
        for (int index = 0; index < 10000; index++){
            password = PasswordGenerator.generatePassword();
            if (passwords.contains(password))
                    index--;
            passwords.add(password);
        }

        System.out.println("expected passwords 10000, generated : " + passwords.size());
        System.out.println(PasswordGenerator.generatePassword());
        System.out.println(PasswordGenerator.generatePassword());
        System.out.println(PasswordGenerator.generatePassword());
        System.out.println(PasswordGenerator.generatePassword());
    }
}