package com.progressoft.passwordGenerator;

public class PasswordGenerator {

    private static PasswordCharacters characters = new PasswordCharacters();
    public static String generatePassword() {

        char[] password = new char[8];
        char character;

        for (int i = 0; i < password.length; i++) {

            character = generateRandom(i);
            if (containsChar(password, character))
                i--;
            else
                password[i] = character;

        }
        return String.valueOf(password);
    }

    private static char generateRandom(int i) {
        if (numberDigit(i))
            return characters.generateNum();
        if (symbolDigit(i))
            return characters.generateSymbol();
        return characters.generateLetter();

    }

    private static boolean symbolDigit(int i) {
        return i <= 3 && !numberDigit(i);
    }

    private static boolean numberDigit(int i) {
        return i % 2 == 0;
    }

    public static boolean containsChar(char[] array, char target) {
        for (char c : array)
            if (c == target)
                return true;
        return false;
    }
}
