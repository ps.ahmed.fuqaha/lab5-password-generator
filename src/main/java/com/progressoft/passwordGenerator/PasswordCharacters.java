package com.progressoft.passwordGenerator;


import java.security.SecureRandom;

public class PasswordCharacters {

        private char[] numbers = new char[10];
        private char[] symbols = new char[]{'_', '#', '$', '%'};
        private char[] letters = new char[26];
        private static SecureRandom secureRandom = new SecureRandom();


    public PasswordCharacters() {
            for (int i = 0; i < 10; i++)
                numbers[i] = ((char) (i+48));

            for (int i = 0; i < 26; i++) {
                letters[i] = ((char) (i + 65));
            }

        }

        public char generateLetter() {
            return letters[secureRandom.nextInt(26)];
        }

        public char generateNum() {
            return numbers[secureRandom.nextInt(10)];
        }

        public char generateSymbol() {
            return symbols[secureRandom.nextInt(4)];
        }
    }

